* [Tokugawa Corp](http://fullmotionvideo.free.fr/phpBB3/index.php) - General information about emulating older japanese games
* [VOGONS](http://www.vogons.org/index.php) - General non-japanese information about running older games on modern operating systems
* [Japanese Computer Emulation Centre](http://www.jcec.co.uk/index.html)
* [Emulation General Wiki](http://emulation.gametechwiki.com/index.php/Main_Page) - A wiki about emulation
* [Japanese PC Compendium](https://japanesepccompendium.blogspot.com/)
* [The Asenheim Project](http://tss.asenheim.org/) - Ports of old JAST releases