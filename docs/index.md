# Eroge Emulation

There's a lot of older eroge that don't work on modern operating systems, so this guide exists to help you figure out how to make them work!

## Running Pre-Windows Eroge

Eroge released on floppy disks, or just generally released for specific platforms.

However most of the emulators are only available on Windows so one day they might stop working as well, which would require a VM to run them...

* <del>[PC-88](emu/pc-88.md)</del>
* <del>[PC-98](emu/pc-98.md)</del>

## Running Old Windows Eroge

Eroge that were generally released in a CD/DVD format, usually released after 1996. These most likely won't work on more modern Windows operating systems, such as Vista or newer.

* [Virtualbox](vm/virtualbox.md)
* <del>[VMWare](vm/vmware.md)</del>