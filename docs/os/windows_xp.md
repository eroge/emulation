# Windows XP Setup

Details on how to setup windows XP, specifically for Eroge.

## The Blue Screen

You'll be greeted with the good old blue setup screen. Simply press `Enter` on your keyboard, followed by `F8` to agree to the licensing agreement.

If you're an expert user you could use the same virtualbox disk image across multiple VMs by partitioning the disk at this screen, but I personally don't see much point in doing so, so you just need to press `Enter` again.

The next screen presents you with filesystem formatting, but the default of `NTFS` is fine so just press `Enter` yet again.

It will then cycle through a few more screens, which actually formats the disk, and copies over the setup files. This should take anywhere from 1 minute to 5+ minutes.

And then the machine will go into the actual WindowsXP stylized setup screen.

## Regional and Language Options

After a while of waiting around, you will finally be prompted with the `Regional and Language Options` screen, which is actually a pretty important screen. The window is split into two halves, and you will need to click on the top half's `Customize...` button. 

A new pop-up will appear, which contains multiple tabs at the top of the window. Click on `Languages`, then at the bottom of the window click the checkbox next to `Install files for East Asian languages`. A new pop-up will appear telling you what it does, so just click `OK`.

You should now navigate to the `Advanced` tab, where you will find settings for non-unicode programs. Change the dropdown to `Japanese`.

**Optional:** If you navigate back to the `Regional Options` tab, you can change the dropdown at the top of the window to `Japanese` to bypass some "Gaijin Blocking DRM" that certain releases incorporate. Most older works do not have this kind of DRM so it's not usually necessary, but is useful to keep in mind if you start playing more modern works inside of the VM as well.

Click `OK` at the bottom of the window to save our current settings and exit the window.

You will now be back at the original `Regional and Language Options` screen, so we will now venture into the lower half's `Details...` button to setup the IME.

The East Asian Languages option you installed earlier setup our Japanese IME automatically for us, but we still have the English IME installed which is totally unnecessary. Click on `English (United States)` in the list at the bottom of the window, then click on `Remove`.

You will now only have the Japanese keyboards installed, so you can click on `OK` to save and exit this window. An error message will appear stating that we can't remove it while in setup, so just click `OK` because it will remove it anyways later.

Click on `Next >` to continue setting up the operating system.

## Personalize your Software

Just give the operating system a random name, and leave organization blank if you want to.

Click on `Next >` to continue setting up the operating system.

## Your Product Key

If you have a product key then this would be the time to enter it. You can move on from this page if you don't currently have one, so just click `Next >` when you're ready. If you didn't enter a product key then you'll get an addition popup to which you'll need to answer `No` to continue.

## Computer Name and Administrator Password

The default computer name is fine unless you're autistic.

You'll likely never need it, but make your Administrator Password and store it in your password manager. Alternatively you can just keep it something simple like "eroge" again, or put it on a textfile on your desktop...

Click on `Next >` to continue setting up the operating system.

## Date and Time Settings

Virtualbox automatically sets the time of the VM to whatever your host operating system is, so it should be set properly.

The timezone will need to be changed to whatever you want. Certain newer games have "anti-gaijin drm" which looks for your timezone to be set to japan, so you might want to set it to `(GMT +09:00) Osaka, Sapporo, Tokyo` since you likely won't need to tell the time from inside of the machine itself.

Click on `Next >` to continue setting up the operating system.

## Networking Settings

The default settings are generally all you need to get it up and working with VirtualBox.

Click on `Next >` to continue setting up the operating system.

## Workgroup or Computer Domain

You likely won't need to setup anything like workgroups for eroge...

Click on `Next >` to continue setting up the operating system.

## Finalizing Installation

You're finally done with the basic setup process, so you'll just have to wait for it to copy over all of the files and reboot.

After the machine reboots, you'll be prompted with a popup saying it'll try to reconfigure your display, so just click `OK`, followed by `OK` once more.

You'll start to see a video, and annoying music will start playing (if you have sound enabled). Additionally your IME will be covering up the bottom right of the screen, so i suggest positioning your mouse onto the "||" located on the lefthand side, clicking on it, then dragging it somewhere else. You should now see a `Next ->` button where the IME was, so click on it.

## Help protect your PC

Windows Update is a little annoying to try and get working nowadays, so mark the checkbox next to `Not Right Now` to disable automatic updates. 

Click on `Next ->` to continue setting up the operating system.

## Checking your internet connectivity

You likely don't have internet setup with virtualbox yet, so you can just click on `Skip >>>`.

## Ready to activate Windows?

Check the box next to `No, remind me every few days`, then click `Next ->`.

## Who will use this computer?

Just input whatever you want into `Your Name` (like "Erogeist"). Keep in mind that your japanese IME is active, so start your input with a capital letter if you want to input English text. Then click `Next ->`.

## Thank You!

You're finally done, so you can click on `Finish ->` to finally boot into the operating system.

# Returning to where you were

If you were following a VirtualMachine setup guide, please use these links to continue where you left off:

* [Virtualbox](../vm/virtualbox.md#windows-xp)

Alternatively just hit "back" in your browser....