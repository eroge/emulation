# Oracle VM VirtualBox®

Installing virtualbox and windowsXP is fairly straightforward as they both have a ton of descriptive help text during the process, but if you're feeling lost at any point this guide will walk you through it step-by-step.

## Prerequisites

* [Virtualbox](https://www.virtualbox.org/) - Choose the correct release for your main operating system.
* Virtualbox Extension Pack from the [download page](https://www.virtualbox.org/wiki/Downloads)
* You will also need to find a disk/iso of WindowsXP somewhere.
    * Trying to buy a copy is literally impossible in 2017...
    * It's possible to find a [copy on MSDN](https://msdn.microsoft.com/en-us/subscriptions/downloads/?errorId=AuthzRuleNoValidClaimsFound&errorIdHash=0937524249#FileId=15551), but those licenses cost literally $500...
    * Microsoft Imagine, and Dreamspark, both don't have XP...
    * TechNet is dead....
    * I'd fucking love it if [winworld](https://winworldpc.com/) would finally support WindowsXP since it's impossible to buy it...

You can also download a pre-made virtualbox disk image with everything set up and ready to go.

## Creating a new virtual machine

After opening up virtualbox, the first step is to click on the big `New` button at the top of the virtualbox window. This will then pop-up the "Guided VM Mode", which will walk you through the basic steps of setting up a new virtual machine one step at a time.

### Name and Operating System

You are welcome to name your new Virtual Machine (VM) whatever you would like to, or you can simply name it "Eroge Box". After naming your VM, you will then need to ensure that the `Type` field is set to `Microsoft Windows`, and the `Version` field is set to `Windows XP (32-bit)`. After that you can click on `Next` at the bottom of the popup to advance.

### Memory size

Virtualbox defaults to a memory size of `192MB` for windowsXP installations, which is fine for most use-cases. You're welcome to increase the amount to 512MB or even 1GB if you're planning to play more modern works inside of your VM as well as an all-in-one solution.

### Hard Disk

You will then be prompted on whether you want to create a "Virtual Hard Disk", which is used as storage for your new operating system. You can just click next, as the default is to `Create a virtual hard disk now`.

#### Hard Disk File Type

A new pop-up will appear, and you should leave it at the default of `VDI (Virtualbox Disk Image)`.

#### Storage on Physical Hard Disk

If you have a lot of excess space on your host operating system, you are welcome to choose `Fixed size` which will give you (slightly) improved performance when installing new eroge, but will likely not ever be noticeable. Otherwise you can just leave this at the default of `Dynamically allocated`, as it will save you space until you actually *need* the space.

#### File Location and Size

The virtual harddisk defaults to the name you set earlier, but you might want to change the location that it is saved to. To do this, you can click on the small folder icon to the right of the textbox; a traditional "save menu" will pop-up, so navigate to where you'd like to save it and then click `save`.

If you're planning on only storing 1 Eroge at a time into the machine, the default maximum of `10.0 GB` is fine. However if you'd like to store *all* of your Eroge into a single machine, you'll need to increase it. If you chose `Dynamically allocated` in the previous step, this is simply the most amount of space you'll ever be allowed to put into the machine, but won't be used up right away. However if you chose `Fixed size` in the previous step, all of this space will immediately become unusable for your host operating system.

A lot of older eroge are around `500-700MB` if they were distributed on CD and have voice, with a few being less than `200MB` without voice. If they were distributed on DVD then they start to become `>1GB` pretty quickly, so expand your maximum size accordingly.

## First Bootup

You should now be back at the main VirtualBox window, with `Eroge Box` (or whatever you named it) displayed in the list. Doubleclick on it to boot it up.

You will be asked to locate your installation disk, either physically inserted into a drive, or located somewhere in your host operating system as a `.iso` file. Click on the small folder to the right of the dropdown box to choose the location of your installation disk, then click `open` after selecting it. Afterwards click on `Start` to finally boot up the machine.

### Windows XP

The Windows XP setup process is pretty self-explanatory, but there's a few setup options we're going to tweak in order to set it up properly for running Eroge.

You can find the detailed setup process [located here](../os/windows_xp.md). Please continue using this guide after you've successfully set up the operating system.

## Quality of Life Setup

A few quality-of-life steps are presented here that are highly recommended before you start embarking on installing all of your eroge.

### Language Bar

You're greeted with your start menu popped open, and the language bar is akwardly floating above the taskbar. At the far right of the language bar, you'll see a small "-" and triangle; click on the "-" to merge the language bar with the taskbar.

Since you've actually performed input, WindowsXP will start bombarding your taskbar with a bunch of annoying help popups, so just keep closing them until they stop.

By default, the language bar is badly positioned on the taskbar and hiding some important information, so rightclick in the middle of the taskbar and select `Lock the Taskbar` to display handlebars next to the language bar. If you click and drag the handlebars to the left, you'll see a button with the letter `A`, which is used to change input method types. Drag it however far you want so you can at least see the `A`, then rightclick the taskbar again and select `Lock the Taskbar` once more.

### Guest Additions CD

Virtualbox comes with a handy CD that will install certain quality-of-life enhancements into windowsXP. To insert the CD, navigate to the top of the VM window where you'll see options like `File`, `Machine`, etc. Click on `Devices`, and at the bottom of that menu you'll see the option `Insert Guest Additions CD image...` so click on that.

A popup should automatically appear, so just keep on clicking `Next >` throughout the entire thing. After it tries to install the software you will eventually receive a popup with a warning message that you can safely ignore, so make sure to click on `Continue Anyway`! And then you'll get another identical popup, so click on `Continue Anyway` again... And then once more...

Once you're finally done with the popups, it will ask you to reboot the machine, so you should just click on `Finish` so it does that.

After you've rebooted, you will be greeted with a few more popups by windows at the bottom right of the screen, which you can safely ignore and close.

This CD enabled better display, mouse, and other drivers to improve overall compatability and seamless scaling with your host operating system. If you were to try and resize your window before doing this step, it would be a small 800x600 display in a large blank window; after installing all of this, it will now seamlessly resize the display to match the size of the window!

### Display Options

Shutdown the machine by either going through the start menu and shutting it down, or clicking on `Machine -> ACPI Shutdown` at the top of the VM window.

Once you're back at the main virtualbox window, rightclick on your VM and click on `Settings`, then navigate to the `Display` tab.

Change the `Video Memory` slider to the maximum of `128MB` since there's no reason not to.

You're also welcome to increase the `Monitor Count`, but it can be a little annoying to manage and will increase the power required to run, so I don't really suggest it.

I highly recommend enabling `Enable 2D Video Acceleration`, and `Enable 3D Accleration` as well since you'll never know when you'll come across works with 3D in them.

You can also enable `Remote Display` or `Screen Recording` in their respective tabs, but neither will be covered in this article.

If you now click `OK` to save your settings and exit the settings menu, then boot back into the machine, it should now be be much more capable of running games.

### Network Capabilities

I *really* don't suggest enabling network capabilities for your machine since it's pretty fucking vulnerable without updates, but if you're truly crazy enough to do so then this is how.

You'll need to shutdown the machine by either going through the start menu and shutting it down, or clicking on `Machine -> ACPI Shutdown` at the top of the VM window.

Once you're back at the main virtualbox window, rightclick on your VM and click on `Settings`.

Head down to the `System` tab on the left, and then click the checkbox next to `Network` in `Boot Order` on the right side of the screen.

Then head down to the `Network` tab on the left, and click into the `Adapter 2` tab. Click the checkbo next to `Enable Network Adapter`, and set the dropdown `Attached to:` to `Bridged Adapter`

You can now click on `OK` to close the settings. If you boot back into your VM you should now be able to directly access the internet from within the machine.

## Activating Windows

If you have *for some reason* set up network capabilities and exposed your VM to the dangers of the internet then you are able to use internet activation, otherwise you will need to use Phone Activation. Alternatively you can just crack the activation process since it's not like you even can even buy a key legally in 2017...

Simply click on the little `Key` icon at the bottom right of the taskbar, or click on `All programs` in the taskbar and it'll be at the top of the program listing.

Assuming you've now activated, or cracked, your windowsXP installation you can head on to the next step.

## Transferring files to the machine

Virtualbox allows a few different methods for actually getting files onto the machine itself.

There's no need to copy over the actual installation media for the games themselves, so this is mostly for additional software like `ITH`.

### Drag and Drop

Likely the easiest method, simply find `Devices` at the top of the VM window, then hover over `Drag and Drop` near the bottom, and select `Host to Guest` or `Bidirectional`. Now all you have to do is drag a folder from your main operating system onto the VM window and it'll be copied to the machine.

### Shared Clipboard

Find `Devices` at the top of the VM window, then hover over `Shared Clipboard` near the bottom, and select `Host to Guest` or `Bidirectional`. Now if you copy a folder/file on your host operating system, you can `ctrl+v` inside of the VM and it should initate a transfer/copy in the VM. I've occasionally had a few problems getting it to work correctly so I don't recommend using it as the default.

### Shared Folders

This method directly exposes your host operating system by mounting a folder to the VM, but you're able to explicitly disable the VM from being able to write to the folder to keep it safe from rogue viruses. You could also use this feature to keep a single copy of ITH/Translation Aggregator on your host system and simply boot them up using the shared folder, instead of keeping track of a separate copy in the VM itself. However you'll still need to install Mecab onto the VM so it's not totally as useful. It's a pretty flexible and handy feature so feel free to experiment with it as needed.

Find `Devices` at the top of the VM window, then hover over `Shared Folders`, and select `Shared Folders Settings...`. A popup will appear with a nearly empty folder list, with some buttons on the right side of the window. Click on the "Folder with a +" to add a new folder location.

`Folder Path` is the location on your host operating system that you'd like to mount, and you can click the "triangle" at the right side of the field, then click "other" to bring up a dialog box to navigate to a specific folder.

`Folder Name` is what it displays as inside of the VM, so name it something simple and easy to remember (like "host files")

`Read Only` is a pretty important field that improves the security of the VM, and I suggest enabling it unless you for some reason need to be able to write to the folder.

`Auto-Mount` and `Make Permanent` should be checked if you want to always have this folder accessible inside of the VM. 

You can then click on `OK` to close the popup, and then `OK` again to close the settings window. 

You will now be required to manually add the network folder in order to access it. However if you've selected `Auto-Mount` and `Make Permanent`, then you can simply reboot the machine to make the network drive show up; otherwise continue on to the next section. Once you've rebooted the machine you can go to the start menu and click on `My Computer`, and you'll find your new shared folder at the bottom of the window with your specified name.

#### Manually adding a network folder

If you want to know how to add a shared network folder, go to the start menu and select `My Computer`. Then on the left side of the window under `Other Places` select `My Network Places`. At the top of the left side of the new window, select `Add a network place`. A new window will popup, so click `Next >`, at which point it'll take a minute to do absolutely nothing. You'll be presented with the only option `Choose another network location`, so make sure that is selected and click `Next >`.

It'll ask for an address, so click on `Browse...`, and in the window that pops up click on the `+` next to `Entire Network`. You should see `Virtualbox Shared Folders` in the list, so expand that by clicking on the `+` next to it. You should only see one option, which should be `¥¥Vboxsvr` (the ¥ sign actually being a backslash), so select the `+` next to that. You will now be presented with all of your shared folders you enabled in the previous section, so you can select any one of those to mount. You can even specifically mount certain subfolders! Once you've selected one, click on `OK` to close the window, followed by `Next >`.

You can now assign a name to this location, but the default is usually fine so click `Next >` again. Click `Finish` and your new network location will appear!

## Snapshots and Clones

This is a good time to explain snapshots and clones, so you can always revert back to this point in case you want to start fresh.

### Snapshots

Snapshots are used to create specific restore points to revert back to, and should be used if you only want everything to be controlled from 1 VM. For example, creating a restore point *right now* to make sure you have a fresh operating system with everything set up would be a pretty good idea, because you won't have to uninstall anything when you're done playing an eroge, you can simply just revert back to the fresh state.

### Clones

Clones are useful if you want to run multiple VMs simultaneously, for example if you want to run 2 or 3 eroge independent of each other. There are two types of clones, `Linked Clones` and `Full Clones`.

A **Linked Clone** is essentially an upgraded snapshot; you can have the VM you created with this guide, then create a new Linked Clone named after an eroge you want to play. It will allow you to simulatenously boot into the original VM and the new VM, and the new VM is simply a snapshot of the old one. However, in order to copy the virtual machine over to a new computer you'll need to transfer both the "base VM" *and* the linked clone.

A **Full Clone** copies the entire disk/settings and makes something entirely different. I'd suggest using a Full Clone if you wanted to run something other than eroge and needed to completely change the system settings but didn't want to run setup again.

## Installing your first Eroge!

Well it's probably taken a while to get to this point, but you're almost done and you'll never have to do the above steps ever again!

### Using Optical Drives

Virtualbox comes with the ability to directly mount any `.iso`, `.cdr`, or `.dmg` file and expose it as a drive in the VM, or you can simply mount any disk drive from the host operating system directly!

#### Direct Mounting

In order to access this feature, navigate to `Devices` at the top of the VM window, hover over `Optical Drives`, then either select `Choose disk image...` or one of your host operating system's disk drives. That menu also stores some recently used files in case you want to quickly re-insert an image you've used previously.

#### Host Drive Passthrough

If your eroge isn't in one of the compatible formats (iso/cdr/dmg), then you'll likely need to mount it on the host operating system using something like [WinCDEmu](http://wincdemu.sysprogs.org/) or equivalent. After installing WinCDEmu, double click on your file in your host operating system, and it should automatically mount it to a virtual drive. Go back to your VM, and `Machine -> Settings` at the top of the machine window. Head down to the `Storage` tab, where you should see your `vdi` harddisk and a CD labelled `Empty`. Click on the CD, and then at the far right of the window click on the CD with the triangle which opens a dropdown. Inside of the menu, select the same drive letter that was created on your host operating system (in my case, "H:"). After selecting it, the CD will change to being named `Host Drive H:`. You can now click `OK` at the bottom of the window to save your settings.

#### Multiple Drives

You can also add additional drives, but in order to do so you have to first entirely shutdown the operating system. Once you've done so and reached the main Virtualbox menu, rightclick on your VM and click on `Settings`.

Navigate to the `Storage` tab. The default settings have a limit of 3 CDs, but bypassing them too much work to explain so google how to setup the sata controller in virtualbox if you need to.

To the right of the `Controler: IDE` entry in the storage tab, you should see a circle (CD) and square (harddisk) with a "+" on them. Once you click on the CD it'll ask you if you want to choose what goes into the drive now, or later. Choose either one and you'll now see two CDs and 1 harddisk in the listing. You can do this one more time for another CD.

### Finishing Up

As soon as you close the window or select an iso, your game's disk should automatically boot up and you should see the installation menu! If it doesn't, in the VM navigate to `My Computer` in the start menu and find the `D:` drive, then double click on it.